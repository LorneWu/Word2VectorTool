import requests
import os
import subprocess
from configparser import ConfigParser
config = ConfigParser()
config.read('setting.ini')
def download_file(url):
    local_filename = url.split('/')[-1]
    # NOTE the stream=True parameter
    r = requests.get(url, stream=True)
    with open(local_filename, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk: # filter out keep-alive new chunks
                f.write(chunk)
                #f.flush() commented by recommendation from J.F.Sebastian
    return local_filename
def sysremove(dic):
    for i in dic:
        if os.path.exists(i):os.remove(i)


def init():
    wikiFile = config.get('setting','resultdir')+'zhwiki-latest-pages-articles.xml.bz2'
    wikiFileTxt = config.get('setting','resultdir')+'wiki_texts.txt'
    wikiFileZh = config.get('setting','resultdir')+'wiki_zh_tw.txt'
    wikiseg = config.get('setting','resultdir')+'wiki_seg.txt'
    wikimodel = config.get('setting','resultdir')+'med250.model.bin'
    if not os.path.exists(wikiFile):
        print('detect wiki file doesnt exists')
        sysremove([wikiFileTxt,wikiFileZh,wikiseg,wikimodel])
        download_file('https://dumps.wikimedia.org/zhwiki/latest/zhwiki-latest-pages-articles.xml.bz2')
    print(wikiFile+" checked ....")
    
    if not os.path.exists(wikiFileTxt):
        print('detect wiki txt doesnt exists')
        sysremove([wikiFileZh,wikiseg,wikimodel])
        os.system('python3 wiki_to_txt.py '+wikiFile)
    print(wikiFileTxt+" checked ....")
    if not os.path.exists(wikiFileZh):
        print('detect wiki txt zh doesnt exists')
        sysremove([wikiseg,wikimodel])
        os.chdir('OpenCC')
        subprocess.run(['opencc','-i',config.get('setting','resultdir')+'wiki_texts.txt','-o',config.get('setting','resultdir')+'wiki_zh_tw.txt'])
        os.chdir("..")
        #os.system('opencc -i wiki_texts.txt -o wiki_zh_tw.txt -c s2tw.json')
    print(wikiFileZh+" checked ....")
    if not os.path.exists(wikiseg):
        print('detect wiki seg doesnt exists')
        sysremove([wikimodel])
        os.system('python3 segment.py')
    print('segment checkted')
    if not os.path.exists(wikimodel):
        print('start training')
        os.system('python3 train.py')
    print('train checked')
    print('finish')
#os.remove('zhwiki-latest-pages-articles.xml.bz2')
init()